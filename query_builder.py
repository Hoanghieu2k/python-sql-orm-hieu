from calendar import month
from peewee import *
from fastapi import FastAPI

app = FastAPI()

db = PostgresqlDatabase(
    'remis_re_dev_v3',  # Required by Peewee.
    user='remisdemo',  # Will be passed directly to psycopg2.
    password='remisdemo',  # Ditto.
    host='103.252.1.131',
    port=5452)  # Ditto.

class BaseModel(Model):
    class Meta:
        database = db

class RealEstate_post(BaseModel):
    id = PrimaryKeyField(unique=True)
    posted_at = DateTimeField()
    type_id = IntegerField()

    class Meta:
        db_table = "RealEstate_post"


class RealEstate_realestate(BaseModel):
    id = PrimaryKeyField(unique=True)

    post_id = ForeignKeyField(RealEstate_post, backref='RealEstate_post')
    
    type_id = IntegerField()

    class Meta:
        db_table ="RealEstate_realestate"

class RealEstate_type(BaseModel):
    id = PrimaryKeyField(unique=True)
    name = CharField()
    type_id = ForeignKeyField(RealEstate_post, backref='RealEstate_post')   

    class Meta:
        db_table ="RealEstate_type"



#get api
#Total dự án theo tháng
@app.get("/get")
async def home():
    total = fn.COUNT(RealEstate_post.type_id).alias('count')
    month = fn.date_part('month', RealEstate_post.posted_at)
    query = (RealEstate_post.select(total, month)
            .join(RealEstate_realestate, on = (RealEstate_post.id == RealEstate_realestate.post_id ))
            .where(fn.date_part('year', RealEstate_post.posted_at) == 2022)
            .group_by(month))
    print(query)
    result = db.execute(query).fetchall()
    return(result)

        


#Total dự án theo tháng
# total = fn.COUNT(RealEstate_post.type_id).alias('count')
# month = fn.date_part('month', RealEstate_post.posted_at)
# query = (RealEstate_post.select(total,month)
#         .join(RealEstate_realestate,on = (RealEstate_post.id == RealEstate_realestate.post_id ))
#         .where(fn.date_part('year', RealEstate_post.posted_at) == 2022)
#         .group_by(month))
# print(query)
# result = db.execute(query)
# for row in result:
#     print(row)

#tổng số lượng bài đăng phân loại theo Loại Bất động và theo từng tháng trong năm 2022
# total_1 = fn.COUNT(RealEstate_post.id).alias('count')
# month_1 = fn.date_part('month', RealEstate_post.posted_at)
# query_1 = (RealEstate_post.select(total_1,month_1,(RealEstate_type.name).alias('Tên loại BĐS'))
#         .join(RealEstate_realestate,on = (RealEstate_realestate.post_id == RealEstate_post.id))
#         .join(RealEstate_type,on = (RealEstate_realestate.type_id == RealEstate_type.id))
#         .where(fn.date_part('year', RealEstate_post.posted_at) == 2022)
#         .group_by((RealEstate_type.name),month_1)
#         )
# print(query_1)
# result_1 = db.execute(query_1)
# for row_1 in result_1:
#     print(row_1)

# #Tổng số lượng bài post trong tháng ̃7/2022
# total_2 = fn.COUNT(RealEstate_post.id).alias('count')
# query_2 = (RealEstate_post.select(total_2)
#             .where((fn.date_part('year',RealEstate_post.posted_at) == 2022) 
#             & (fn.date_part('month',RealEstate_post.posted_at) == 7)) )
# print(query_2)
# result_2 = db.execute(query_2)
# for row_2 in result_2:
#     print(row_2)














