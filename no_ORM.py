import psycopg2
from fastapi import FastAPI

app = FastAPI()

#establishing the connection
conn = psycopg2.connect(
   database="remis_re_dev_v3",
   user='remisdemo',
   password='remisdemo',
   host='103.252.1.131',
   port= '5452'
)
#Setting auto commit false
conn.autocommit = True

#Creating a cursor object using the cursor() method
cursor = conn.cursor()
cursor1 = conn.cursor()
cursor2= conn.cursor()
# Retrieving data


# getApi
@app.get("/get")
async def home():
   cursor.execute('''
      select  count(rp1.type_id) AS "Tổng Số Lượng Bài Đăng" ,  DATE_PART('month',posted_at) AS "Month" ,REC2.name AS "Tên môi giới"
      from "RealEstate_post" rp1
      INNER JOIN "RealEstate_profile" REc
      on rp1.profile_id = REc.id
      Inner Join "RealEstate_profiletype" REC2
      ON REc.type_id = REC2.id
      where  DATE_PART('year',posted_at) = 2022
      GROUP BY REC2.name, "Month"
      ORDER BY "Tổng Số Lượng Bài Đăng"  DESC
   ''')
   result = cursor.fetchall()
   return (result)

@app.get("/get1")
async def home1():
   cursor1.execute('''select "number_of_posts",RS.name
      from
      (select RP.source_id, count(*) as number_of_posts
         from "RealEstate_post" RP
         group by "source_id") as id
      INNER join "RealEstate_source" RS
      ON id."source_id" = RS."id"
      ORDER BY "number_of_posts" DESC
   ''')
   result2 = cursor1.fetchall()
   return (result2)

@app.get("/get2")
async def home2():
   cursor2.execute('''
   SELECT count(RR.type_id) AS "Tổng Số Lượng Bài Đăng" ,
      DATE_PART('month',posted_at) AS "Month" ,
      RT.name AS "Tên loại BĐS"
   from "RealEstate_realestate" RR
   INNER JOIN "RealEstate_post" RP
   on RR.post_id = RP.id
   Inner Join "RealEstate_type" RT
   ON RR.type_id = RT.id
   where  DATE_PART('year',posted_at) = 2022
   GROUP BY RT.name, "Month"
   ORDER BY "Tổng Số Lượng Bài Đăng"  DESC
''')
   result3 = cursor2.fetchall()
   return (result3)

#Commit your changes in the database
# conn.commit()  #khi nào thay đổi dữ liệu mới sử dụng commit()
#Closing the connection
# conn.close() 
#Fetching 1st row from the table
