from sqlalchemy import  Column, DateTime, ForeignKey,  Integer, create_engine,String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker,relationship
from sqlalchemy import func, distinct
from sqlalchemy import func
from datetime import date 
from fastapi import FastAPI

app = FastAPI()

Base = declarative_base()
engine = create_engine('postgresql://remisdemo:remisdemo@103.252.1.131:5452/remis_re_dev_v3',echo = True)
Base.metadata.create_all(bind = engine )
Session = sessionmaker(bind = engine)
session = Session()

class Demand(Base):
   __tablename__ = "RealEstate_demand"
   id = Column (Integer, primary_key = True )
   name = Column (String(50))

class Post(Base):
    __tablename__ = "RealEstate_post"
    id = Column (Integer, primary_key = True )
    demand_id = Column(Integer,ForeignKey('RealEstate_demand.id'))
    posted_at = Column(DateTime)
 

# -====================-
# Product1 = session.query(Post).count()
# # print(Product1)
# # Product2= session.query(Post).filter(extract('year', Post.posted_at)==2022).filter(extract('month', Post.posted_at)==7).limit(10)
# # print(Product2)
# Product3 = session.query(Product1).filter(func.year(Post.posted_at) == 2022).all()
# print(Product3)
# # a = engine.execute(Product3).fetchall()
# # return(a)

result = session.query(distinct(func.date_part('YEAR',Post.posted_at)))
for row in result:
    print(row[0])


# @app.get("/get")
# async def home():
#     Product1 = session.query(Post).filter(Post.posted_at == "2022-02-28").count()
#     return("demand_id:",Product1)








